#Makefile for MQTT 

BUILD_MODE=debug
CFLAGS=-Wall -pedantic

PROJECT_ROOT = $(dir $(abspath $(lastword $(MAKEFILE_LIST))))

BIN_DIR=bin
LIB_DIR=lib
SRC_DIR=src
OBJS=$(patsubst $(SRC_DIR)/%.c,$(BIN_DIR)/%.o,$(wildcard $(SRC_DIR)/*.c))
#$(info cc:$(CC) c:$(wildcard $(SRC_DIR)/*.c) objs:$(OBJS))

ifeq ($(BUILD_MODE),debug)
	CFLAGS += -g
else ifeq ($(BUILD_MODE),run)
	CFLAGS += -O2
endif


$(BIN_DIR)/mqtt_p: $(BIN_DIR)	$(OBJS) 
	$(CC) -o $(BIN_DIR)/mqtt_p $(OBJS) -lpaho-mqtt3c -lpthread -lcmocka -lcjson

$(BIN_DIR):
	mkdir $(BIN_DIR)
		
$(BIN_DIR)/%.o:	$(SRC_DIR)/%.c
	$(CC) -o $@ -c $<  $(CFLAGS)

clean:
	rm -fr $(BIN_DIR)

all: clean $(BIN_DIR)/mqtt_p

.PHONY: clean all

	