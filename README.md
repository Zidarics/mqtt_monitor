# MQTT demo application

This application checks the target machine parameters and one of these parameters are out of the config rnage it will send an alarm by mqtt. The config is coming from MQTT or local config file or predefined constants. 
Application contains tests to check the submodules. 

## Submodules
### config
Config module can read a local file contains name=value pairs for configuration. 
### mqtt
This module subscribes to "config" topic and send alarm to "alarm" topic if any parameters are out of ranges
### main logic
This modul runs a checking periodically and checking the value are out of range. It sends an alarm if it happens
### utilities
Utilities for json conversion


## Libraries:
* libpaho-mqtt1.3
* libpaho-mqtt-dev
* libcjson1
* libcjson-dev (cJSON: @see https://github.com/DaveGamble/cJSON )
## Useful links
* https://cjson.docsforge.com/
* https://linuxhint.com/list_of_linux_syscal
## 
Usage of application:
## As testing</h2> 
* 	First set the TEST to defined in const.h.
* 	Compile and run or debug app.
* 	It will test opsys.c and config.c and finally mqtt. Mqtt is a little bit tricky, because it creates a test config and publish it
*  to config topic. Because test subscribed to config topic, it will be coming a couple of ms later as a config.
 The best method to check how does it works:
 * Checking the log as root like this in command line:
```
tail -f /var/log/syslog | grep MQTT
```
You can see the syslog continuosly, and you can leave it with ^C
* Debugging, don't hesitate, th ebest method to learn programming is debugging.
## As application
In this case set TEST variable in const.h to undef and recompile and run or debug applicaiton. First it read the configuration and creates a thread to checking system status.
