/*
 * config.c
 *
 *  Created on: Apr 15, 2021
 *      Author: zamek
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/queue.h>
#include <stdbool.h>


#include "macros.h"
#include "util.h"

#define CFG_DELIMITER "="
#define VALUE_SIZE 32
#define MAX_LINE_SIZE 128

struct cfg_item_t {
	char *name;
	char *value;
	TAILQ_ENTRY(cfg_item_t) next;
};

static struct {
    bool initialized;
    bool modified;
	TAILQ_HEAD(cfg_head, cfg_item_t) head;
} config;

static void add_entry(const char *name, const char *value) {
	if (!(name&&*name))
		return;
	struct cfg_item_t *item;
	MALLOC(item, sizeof(struct cfg_item_t));
	item->name=strndup(name, VALUE_SIZE);
	item->value=value?strndup(value, VALUE_SIZE):NULL;
	TAILQ_INSERT_HEAD(&config.head, item, next);
}

static void process_line(char *line) {
	if (!(line&&*line))
		return;

	char *name=strtok(line, CFG_DELIMITER);
	char *value=strtok(NULL, CFG_DELIMITER);
	if (name&&*name)
		add_entry(name, value?value:NULL);
}

int cfg_init(const char *name, bool accept_not_exists){
	if (!(name&&*name&&!(config.initialized)))
		return EXIT_FAILURE;

	TAILQ_INIT(&config.head);
	FILE *file=fopen(name, "r");
	if (!file) {
		if (accept_not_exists) {
			syslog(LOG_ERR, "cfg init filename:%s,  %s (%d)", name, strerror(errno), errno);
			goto exit;
		}
		return EXIT_FAILURE;
	}

	char buffer[MAX_LINE_SIZE];
	while(fgets(buffer, MAX_LINE_SIZE, file)) {
		process_line(chomp(buffer));
	}

	fclose(file);
exit:
	config.initialized=true;
	return EXIT_SUCCESS;
}

int cfg_deinit() {
	struct cfg_item_t *it;
	if (!config.initialized)
		return EXIT_FAILURE;
	while(!TAILQ_EMPTY(&config.head)) {
		it=TAILQ_FIRST(&config.head);
		TAILQ_REMOVE(&config.head, it, next);
		FREE(it->name);
		FREE(it->value);
		FREE(it);
	}
	config.initialized=false;
	return EXIT_SUCCESS;
}

int cfg_save(const char *name) {
	if (!(name&&*name&&config.initialized))
		return EXIT_FAILURE;

	FILE *file=fopen(name, "w");
	if (!file) {
		syslog(LOG_ERR, "cfg save filename:%s,  %s (%d)", name, strerror(errno), errno);
		return EXIT_FAILURE;
	}

	struct cfg_item_t *it;
	char buffer[MAX_LINE_SIZE];

	TAILQ_FOREACH(it, &config.head, next) {
		snprintf(buffer, MAX_LINE_SIZE, "%s=%s\n", it->name, it->value?it->value:"");
		fputs(buffer, file);
	}
	fclose(file);
	return EXIT_SUCCESS;
}

static struct cfg_item_t *find_key(const char *key) {
	if (!(key&&*key&&config.initialized))
		return NULL;

	struct cfg_item_t *it;
	TAILQ_FOREACH(it, &config.head, next) {
		if (strcmp(it->name, key)==0)
			return it;
	}
	return NULL;
}

static inline struct cfg_item_t *find(const char *name) {
	return (name&&*name) ? find_key(name):NULL;
}

int cfg_get_int(const char *name, int def){
	struct cfg_item_t *it=find(name);
	return it && it->value ? atoi(it->value) : def;
}

long cfg_get_long(const char *name, long def){
	struct cfg_item_t *it=find(name);
	return it && it->value ? atol(it->value) : def;
}

long long cfg_get_long_long(const char *name, long long def){
	struct cfg_item_t *it=find(name);
	return it && it->value ? atoll(it->value) : def;
}

float cfg_get_float(const char *name, float def){
	struct cfg_item_t *it=find(name);
	return it && it->value ? atof(it->value) : def;
}

double cfg_get_double(const char *name, double *value, double def){
	struct cfg_item_t *it=find(name);
	return it && it->value ? atof(it->value) : def;
}

void cfg_get_string(const char *name, char *value, int length,  const char *def){
	struct cfg_item_t *it=find(name);
	strncpy(value, it && it->value?it->value:def, length);
}

#define SET_NUMBER(name, value, format)    					\
	do {													\
		config.modified=true;								\
		struct cfg_item_t *it=find(name);					\
		char *val_buffer;									\
		MALLOC(val_buffer, VALUE_SIZE);						\
		snprintf(val_buffer, VALUE_SIZE, format, value); 	\
		if (it) {											\
			if (it->value)									\
				FREE(it->value);							\
			it->value=val_buffer;							\
			return EXIT_SUCCESS;							\
		}													\
		add_entry(name, val_buffer);						\
		return EXIT_SUCCESS;								\
	} while(0)

int cfg_set_int(const char *name, int value){
	SET_NUMBER(name, value, "%d");
}

int cfg_set_long(const char *name, long value){
	SET_NUMBER(name, value, "%ld");
}

int cfg_set_long_long(const char *name, long long value){
	SET_NUMBER(name, value, "%lld");
}

int cfg_set_float(const char *name, float value){
	SET_NUMBER(name, value, "%f");
}

int cfg_set_double(const char *name, double value){
	SET_NUMBER(name, value, "%f");
}

int cfg_set_string(const char *name, const char *value, int length){
	struct cfg_item_t *it=find(name);
	if (it) {
		if (it->value)
			FREE(it->value);
		it->value=strdup(value);
		return EXIT_SUCCESS;
	}
	add_entry(name, value);
	return EXIT_SUCCESS;
}
