/*
 * config.h
 *
 *  Created on: Apr 15, 2021
 *      Author: zamek
 */

#ifndef SRC_CONFIG_H_
#define SRC_CONFIG_H_

#include <stdbool.h>

int cfg_init(const char *file, bool accept_not_exists);

int cfg_deinit();

int cfg_save(const char *name);

int cfg_get_int(const char *name, int def);

long cfg_get_long(const char *name, long def);

long long cfg_get_long_long(const char *name, long long def);

float cfg_get_float(const char *name, float def);

double cfg_get_double(const char *name, double def);

void cfg_get_string(const char *name, char *value, int length,  const char *def);

int cfg_set_int(const char *name, int value);

int cfg_set_long(const char *name, long value);

int cfg_set_long_long(const char *name, long long value);

int cfg_set_float(const char *name, float value);

int cfg_set_double(const char *name, double value);

int cfg_set_string(const char *name, const char *value, int length);

#endif /* SRC_CONFIG_H_ */
