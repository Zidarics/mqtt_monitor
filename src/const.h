/*
 * const.c
 *
 *  Created on: Apr 10, 2021
 *      Author: zamek
 */

#ifndef SRC_CONST_H_
#define SRC_CONST_H_

#define APP_NAME "MQTT"
#undef TEST
#define DEBUG

#define CONFIG_FILE_NAME APP_NAME "mqtt.cfg"

#define DEF_MAX_LOAD1 10
#define DEF_MAX_LOAD5 5
#define DEF_MAX_LOAD15 3
#define DEF_MIN_FREE_RAM 1000000
#define DEF_MIN_FREE_SWAP 0
#define DEF_MAX_PROCESSES 10000
#define DEF_CHECK_INTERVAL_SEC 5

#define MIN_MAX_LOAD1 5
#define MIN_MAX_LOAD5 1
#define MIN_MAX_LOAD15 1
#define MIN_MIN_FREE_RAM 10000
#define MIN_MIN_FREE_SWAP 0
#define MIN_MAX_PROCESSES 10
#define MIN_CHECK_INTERVAL_SEC 3

#define MAX_MAX_LOAD1 50
#define MAX_MAX_LOAD5 50
#define MAX_MAX_LOAD15 50
#define MAX_MIN_FREE_RAM 100000000
#define MAX_MIN_FREE_SWAP 20000000
#define MAX_MAX_PROCESSES 1000
#define MAX_CHECK_INTERVAL_SEC 30

#define KEY_MAX_LOAD1 "maxLoad1"
#define KEY_MAX_LOAD5 "maxLoad5"
#define KEY_MAX_LOAD15 "maxLoad15"
#define KEY_MIN_FREE_RAM "minFreeRam"
#define KEY_MIN_FREE_SWAP "minFreeSwap"
#define KEY_MAX_PROCESSES "maxProcessess"
#define KEY_CHECK_INTERVAL_SEC "checkIntervalSec"

#define TOPIC_CONFIG "config"
#define TOPIC_ALARM "alarm"

#define SUBSCRIBE_QOS 1
#define PUBLISH_QOS 1

#define JSON_LABEL_SYSINFO "sysinfo"
#define JSON_LABEL_UPTIME "uptime"
#define JSON_LABEL_LOADS "loads"
#define JSON_LABEL_LOAD1 "load1"
#define JSON_LABEL_LOAD5 "load5"
#define JSON_LABEL_LOAD15 "load15"
#define JSON_LABEL_TOTAL_RAM "totalRam"
#define JSON_LABEL_FREE_RAM "freeRam"
#define JSON_LABEL_SHARED_RAM "sharedRam"
#define JSON_LABEL_BUFFER_RAM "bufferRam"
#define JSON_LABEL_TOTAL_SWAP "totalSwap"
#define JSON_LABEL_FREE_SWAP "freeSwap"
#define JSON_LABEL_PROCS "procs"
#define JSON_LABEL_TOTAL_HIGH_MEM_SIZE "totalHighMemSize"
#define JSON_LABEL_TOTAL_HIGH_MEM_FREE "totalHighMemFree"
#define JSON_LABEL_MEM_UNIT_SIZE "memUnitSize"
#define JSON_LABEL_TIMESTAMP "timestamp"
#define JSON_LABEL_MACHINE_ID "machineId"
#define JSON_LABEL_HOST_NAME "hostName"
#define JSON_LABEL_CHECK_INTERVAL_SEC "checkIntervalSec"

typedef struct {
    int max_load1;
	int max_load5;
	int max_load15;
	long min_free_ram;
	long min_free_swap;
	int max_processes;
	int check_interval_sec;
} config_t;

#endif /* SRC_CONST_H_ */
