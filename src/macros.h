/*
 * macros.h
 *
 *  Created on: Apr 10, 2021
 *      Author: zamek
 */

#ifndef SRC_MACROS_H_
#define SRC_MACROS_H_

#include <stdio.h>
#include <syslog.h>
#include <string.h>
#include <errno.h>

#define MALLOC(ptr,size) 	\
	do {				 	\
		ptr=malloc(size); 	\
		if (!ptr)			\
			abort();		\
	}while(0)

#define FREE(ptr) 			\
	do {					\
		free(ptr);			\
		ptr=NULL;			\
	} while(0)

#if defined(DEBUG)
#define LOGD(msg, ...)  syslog(LOG_DEBUG, msg, ##__VA_ARGS__)
#else
#define LOGD(...)
#endif


#endif /* SRC_MACROS_H_ */
