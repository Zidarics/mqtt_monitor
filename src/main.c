/**
 * @mainpage
 *
 * Libraries:
 *      libpaho-mqtt1.3
 * 		libpaho-mqtt-dev
 *      libcjson1
 * 		libcjson-dev (cJSON: @see https://github.com/DaveGamble/cJSON )
 *      https://cjson.docsforge.com/
 *
 * 		https://linuxhint.com/list_of_linux_syscal
 *
 * Usage of application:
 * <ul>
 * 	<li><h2>As testing</h2>
 * 	<p>
 * 		<ul>
 * 			<li>First set the TEST to defined in const.h.</li>
 * 			<li>Compile and run or debug app.</li>
 * 			<li>It will test opsys.c and config.c and finally mqtt. Mqtt is a little bit tricky, because it creates a test config and publish it
 * 			to config topic. Because test subscribed to config topic, it will be coming a couple of ms later as a config.
 * 			</li>
 * 	</p>
 * 	<p>The best method to check how does it works:
 * 		<ul>
 * 			<li>Checking the log as root like this in command line:
 * 			<code>
 * 				tail -f /var/log/syslog | grep MQTT
 * 			</code>
 * 			You can see the syslog continuosly, and you can leave it with ^C
 * 			</li>
 * 			<li>Debugging, don't hesitate, th ebest method to learn programming is debugging.</li>
 * 		</ul>
 * 	</p>
 *
 * 	</li>
 * 	<li><h2>As application<h2>
 * 	<p>In this case set TEST variable in const.h to undef and recompile and run or debug applicaiton. First it read the configuration and
 * 	creates a thread to checking system status.</p>
 * 	</li>
 * </ul>
 *
 */
#include <stdio.h>
#include <stdlib.h>
#include <syslog.h>
#include <pthread.h>
#include <unistd.h>
#include <string.h>
#include <time.h>
#include "util.h"

#include "const.h"
#include "test.h"
#include "mqtt.h"
#include "config.h"
#include "opsys.h"


static config_t config;
static pthread_t processor_thread;
static char hostname[64];
static char machine_id[128];
static struct sysinfo info;

static void set_log(){
	setlogmask(LOG_UPTO(LOG_DEBUG));
	openlog(APP_NAME, LOG_CONS | LOG_PID | LOG_NDELAY, LOG_LOCAL1);
}


static int mqtt_subscribe_callback(const char *topic, char * payload, int payload_length) {
	if (!(topic&&payload&&payload_length>0))
		return EXIT_FAILURE;

	if (strcmp(topic, TOPIC_CONFIG)==0) {
		return process_config(&config, payload, payload_length);
	}

	syslog(LOG_WARNING, "Unknown topic coming:%.*s", payload_length, payload);
	return EXIT_FAILURE;
}


/**

{
	"timestamp": 1234567,
	"machineId": "0x345axfe",
	"hostName" : "zamek",
    "load1": 10,
    "load5":  12,
    "load15" : 5,
    "freeRam": 10000,
    "freeSwap" : 1000000,
    "processes" : 1000,
}

 */
static void send_mqtt_alarm() {
	syslog(LOG_DEBUG, "Enter send_mqtt_alarm");
	cJSON *root=cJSON_CreateObject();

	time_t now;
	time(&now);

	if (!(cJSON_AddNumberToObject(root, JSON_LABEL_TIMESTAMP, now) &&
		  cJSON_AddStringToObject(root, JSON_LABEL_MACHINE_ID, machine_id) &&
		  cJSON_AddStringToObject(root, JSON_LABEL_HOST_NAME, hostname)))
		goto exit;


	if (info.loads[0]>=config.max_load1) {
		if (!cJSON_AddNumberToObject(root, JSON_LABEL_LOAD1, info.loads[0]))
			goto exit;
	}

	if (info.loads[1]>=config.max_load5) {
		if (!cJSON_AddNumberToObject(root, JSON_LABEL_LOAD5, info.loads[1]))
			goto exit;
	}

	if (info.loads[2]>=config.max_load15) {
		if (!cJSON_AddNumberToObject(root, JSON_LABEL_LOAD15, info.loads[2]))
			goto exit;
	}

	if (info.freeram<config.min_free_ram) {
		if (! cJSON_AddNumberToObject(root, JSON_LABEL_FREE_RAM, info.freeram))
			goto exit;
	}

	if (info.freeswap<config.min_free_swap) {
		if (! cJSON_AddNumberToObject(root, JSON_LABEL_FREE_SWAP, info.freeswap))
			goto exit;
	}

	if (info.procs>=config.max_processes) {
		if (! cJSON_AddNumberToObject(root, JSON_LABEL_PROCS, info.procs))
			goto exit;
	}

	char *json=cJSON_Print(root);
	if (!json)
		goto exit;

	if (mqtt_send_topic(TOPIC_ALARM, json)==EXIT_SUCCESS)
		syslog(LOG_INFO, "Alarm sent");
	else
		syslog(LOG_WARNING, "Alarm send failure");

	cJSON_free(json);
	cJSON_Delete(root);
	return;

exit:
	syslog(LOG_ERR, "send_mqtt_alarm json create error");
	cJSON_Delete(root);
}

void checking() {
	syslog(LOG_DEBUG, "Enter checking");

	if ( (info.loads[0]<=config.max_load1) &&
		 (info.loads[1]<=config.max_load5) &&
		 (info.loads[2]<=config.max_load15) &&
		 (info.freeram>=config.min_free_ram) &&
		 (info.freeswap>=config.min_free_swap) &&
		 (info.procs<=config.max_processes))
		return;

	send_mqtt_alarm();
}

static void *thread_func(void *args) {
	syslog(LOG_DEBUG, "Enter thread_func");
	while(true) {
		syslog(LOG_DEBUG, "Getting sysinfo");
		if (os_get_sysinfo(&info)!=EXIT_SUCCESS)
			syslog(LOG_WARNING, "sysinfo failure");
		else
			checking();
		sleep(config.check_interval_sec);
	}
	return NULL;
}

static int start() {
	syslog(LOG_DEBUG, "Enter start");
	if (mqtt_init()!=EXIT_SUCCESS) {
		syslog(LOG_ERR, "MQtt init failed");
		return EXIT_FAILURE;
	}

	mqtt_set_subscribe_callback(mqtt_subscribe_callback);

	if (cfg_init(CONFIG_FILE_NAME, true)!=EXIT_SUCCESS) {
		syslog(LOG_ERR, "Cannot open config:%s",CONFIG_FILE_NAME);
		goto error_exit;
	}

	config.max_load1=cfg_get_int(KEY_MAX_LOAD1, DEF_MAX_LOAD1);
	config.max_load5=cfg_get_int(KEY_MAX_LOAD5, DEF_MAX_LOAD5);
	config.max_load15=cfg_get_int(KEY_MAX_LOAD15, DEF_MAX_LOAD15);
	config.max_processes=cfg_get_int(KEY_MAX_PROCESSES, DEF_MAX_PROCESSES);
	config.min_free_ram=cfg_get_long(KEY_MIN_FREE_RAM, DEF_MIN_FREE_RAM);
	config.min_free_swap=cfg_get_long(KEY_MIN_FREE_SWAP, DEF_MIN_FREE_SWAP);
	config.check_interval_sec=cfg_get_long(KEY_CHECK_INTERVAL_SEC, DEF_CHECK_INTERVAL_SEC);

	if (os_get_machine_id(machine_id, 128)!=EXIT_SUCCESS) {
		syslog(LOG_ERR, "get machine id failure");
		goto error_exit;
	}

	if (os_get_host_name(hostname, 64)!=EXIT_SUCCESS) {
		syslog(LOG_ERR, "get hostname failure");
		goto error_exit;
	}

	pthread_create(&processor_thread, NULL, thread_func, NULL);
	pthread_join(processor_thread, NULL);

	cfg_deinit();
	mqtt_deinit();
	return EXIT_SUCCESS;

error_exit:
    cfg_deinit();
	mqtt_deinit();
	return EXIT_FAILURE;
}

int main(int argc, char **argv) {
	set_log();
	int res=EXIT_SUCCESS;
#ifdef TEST
	test();
#else
	res=start();
#endif

	closelog();
	return res;
}
