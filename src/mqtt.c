/*
 * mqtt.c
 *
 *  Created on: Apr 12, 2021
 *      Author: zamek
 */


#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <syslog.h>
#include <MQTTClient.h>

#include "mqtt.h"
#include "const.h"

#define TAG "mqtt"

#define MQTT_SERVER  "tcp://zamek.mik.pte.hu:1883"
#define CLIENTID    "machine_client"
#define MQTT_USER_NAME "ptemik"
#define MQTT_PASSWORD "esp32"

static struct {
	int sent_messages;
	int received_messages;
	int send_errors;
    MQTTClient client;
	bool initialized;
	mqtt_subscribe_callback_t subscribe_cb;
} mqtt;

static int connect() {
    MQTTClient_connectOptions conn_opts = MQTTClient_connectOptions_initializer;
    int rc;
    conn_opts.keepAliveInterval = 20;
    conn_opts.cleansession = 1;
    conn_opts.username=MQTT_USER_NAME;
    conn_opts.password=MQTT_PASSWORD;

	if ((rc = MQTTClient_connect(mqtt.client, &conn_opts)) != MQTTCLIENT_SUCCESS)
    {
        printf("Failed to connect, return code %d\n", rc);
        return EXIT_FAILURE;
    }

	return EXIT_SUCCESS;
}

static void connection_lost(void* context, char* cause) {
	connect();
}

static int message_arrived(void* context, char* topicName, int topicLen, MQTTClient_message* message) {
	if (mqtt.subscribe_cb) {
		mqtt.subscribe_cb(topicName, message->payload, message->payloadlen);
		return MQTTCLIENT_SUCCESS;
	}
	syslog(LOG_WARNING, "Incoming mqtt message, but no callback set");
	return MQTTCLIENT_SUCCESS;
}

static void delivery_complete(void* context, MQTTClient_deliveryToken dt) {
	syslog(LOG_INFO, "Mqtt message delivery complete: %d ", dt);
}


int mqtt_init() {
	if (mqtt.initialized)
		return EXIT_FAILURE;

	bzero(&mqtt, sizeof(mqtt));

	int rc= MQTTClient_create(&mqtt.client, MQTT_SERVER, CLIENTID,
	                                  MQTTCLIENT_PERSISTENCE_NONE, NULL);
	if (rc!=MQTTCLIENT_SUCCESS) {
		syslog(LOG_ERR, "mawtt create failed:%d", rc);
		return EXIT_FAILURE;
	}

	rc=MQTTClient_setCallbacks(mqtt.client, NULL, connection_lost, message_arrived, delivery_complete);
    if (rc!=MQTTCLIENT_SUCCESS) {
    	syslog(LOG_ERR, "set_callbacks failed:%d", rc);
    	return EXIT_FAILURE;
    }

	if (connect()!=EXIT_SUCCESS) {
		syslog(LOG_ERR, "connect failed");
		return EXIT_FAILURE;
	}


    rc=MQTTClient_subscribe(mqtt.client, TOPIC_CONFIG, SUBSCRIBE_QOS);
    if (rc!=MQTTCLIENT_SUCCESS) {
    	syslog(LOG_ERR, "Cannot subscribe to topic %s, err:%d", TOPIC_CONFIG, rc);
    	return EXIT_FAILURE;
    }
    syslog(LOG_INFO, "TOPIC:%s subscribed", TOPIC_CONFIG);

	mqtt.initialized=true;
	return EXIT_SUCCESS;
}

int mqtt_deinit() {
	if (!mqtt.initialized)
		return EXIT_FAILURE;
	mqtt.initialized=false;
    MQTTClient_disconnect(mqtt.client, 10000);
    MQTTClient_destroy(&mqtt.client);
    mqtt.subscribe_cb=NULL;
	return EXIT_SUCCESS;
}


int mqtt_send_topic(const char *topic, char *payload) {
	if (!(topic&&payload&&mqtt.initialized))
		return EXIT_FAILURE;

	 MQTTClient_message msg=MQTTClient_message_initializer;
	 MQTTClient_token token;
	 msg.payload=payload;
	 msg.payloadlen=strlen(payload);
	 msg.qos=1;
	 msg.retained=0;

	 MQTTClient_publishMessage(mqtt.client, topic, &msg, &token);
	 int rc=MQTTClient_waitForCompletion(mqtt.client, token, 10000L);
	 if (rc==MQTTCLIENT_SUCCESS) {
		 ++mqtt.sent_messages;
		 return EXIT_SUCCESS;
	 }
	 ++mqtt.send_errors;
	 return EXIT_FAILURE;
}

int mqtt_set_subscribe_callback(mqtt_subscribe_callback_t cb) {
	mqtt.subscribe_cb=cb;
	return mqtt.initialized ? EXIT_SUCCESS : EXIT_FAILURE;
}
