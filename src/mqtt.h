/*
 * mqtt.h
 *
 *  Created on: Apr 12, 2021
 *      Author: zamek
 */

#ifndef SRC_MQTT_H_
#define SRC_MQTT_H_

typedef int (*mqtt_subscribe_callback_t)(const char *topic, char * payload, int payload_length);

int mqtt_init();

int mqtt_deinit();

int mqtt_set_subscribe_callback(mqtt_subscribe_callback_t cb);

int mqtt_send_topic(const char *topic, char *payload);


#endif /* SRC_MQTT_H_ */
