	/*
 * opsys.c
 *
 *  Created on: Apr 12, 2021
 *      Author: zamek
 */

#include <stdio.h>
#include <stdlib.h>
#include <syslog.h>
#include <sys/sysinfo.h>
#include <cjson/cJSON.h>
#include "macros.h"

#define ID_FILE "/etc/machine-id"
#define HOST_NAME_FILE "/etc/hostname"

#define MIN_BUFFER_LENGTH 64
#define MIN_JSON_BUFFER_LENGTH 255

#define JSON_LABEL_SYSINFO "sysinfo"
#define JSON_LABEL_UPTIME "uptime"
#define JSON_LABEL_LOADS "loads"
#define JSON_LABEL_LOAD1 "load1"
#define JSON_LABEL_LOAD5 "load5"
#define JSON_LABEL_LOAD15 "load15"
#define JSON_LABEL_TOTAL_RAM "totalRam"
#define JSON_LABEL_FREE_RAM "freeRam"
#define JSON_LABEL_SHARED_RAM "sharedRam"
#define JSON_LABEL_BUFFER_RAM "bufferRam"
#define JSON_LABEL_TOTAL_SWAP "totalSwap"
#define JSON_LABEL_FREE_SWAP "freeSwap"
#define JSON_LABEL_PROCS "procs"
#define JSON_LABEL_TOTAL_HIGH_MEM_SIZE "totalHighMemSize"
#define JSON_LABEL_TOTAL_HIGH_MEM_FREE "totalHighMemFree"
#define JSON_LABEL_MEM_UNIT_SIZE "memUnitSize"


static int read_short_file(const char *name, char *buffer, int buffer_length) {
	if (!(name&&buffer&&buffer_length>=MIN_BUFFER_LENGTH))
		return EXIT_FAILURE;

	FILE *file=fopen(name, "r");
	if (!file) {
		syslog(LOG_ERR, "file open error, %s,  %s (%d)", name, strerror(errno), errno);
		return EXIT_FAILURE;
	}

	fgets(buffer,  buffer_length, file);
	fclose(file);
	return EXIT_SUCCESS;
}

/**
 * Allocates memory!
 */
int os_get_machine_id(char *buffer, int buffer_length) {
	return read_short_file(ID_FILE, buffer, buffer_length);
}

/**
 * Allocates memory
 */
int os_get_host_name(char *buffer, int buffer_length) {
	return read_short_file(HOST_NAME_FILE, buffer, buffer_length);
}

int os_get_sysinfo(struct sysinfo *info) {
	if (!info)
		return EXIT_FAILURE;

	sysinfo(info);
	float f_load = (1 << SI_LOAD_SHIFT);
	for (int i=0;i<3;++i)
		info->loads[i]/=f_load;
	return EXIT_SUCCESS;
}

int os_add_sys_info_json(struct sysinfo *info, cJSON *root) {
	if (!(info&&root))
		return EXIT_FAILURE;

	if (! (cJSON_AddNumberToObject(root, JSON_LABEL_UPTIME, info->uptime) &&
		   cJSON_AddNumberToObject(root, JSON_LABEL_TOTAL_RAM, info->totalram) &&
		   cJSON_AddNumberToObject(root, JSON_LABEL_FREE_RAM, info->freeram) &&
		   cJSON_AddNumberToObject(root, JSON_LABEL_SHARED_RAM, info->sharedram) &&
		   cJSON_AddNumberToObject(root, JSON_LABEL_BUFFER_RAM, info->bufferram) &&
		   cJSON_AddNumberToObject(root, JSON_LABEL_TOTAL_SWAP, info->totalswap) &&
		   cJSON_AddNumberToObject(root, JSON_LABEL_FREE_SWAP, info->freeswap) &&
		   cJSON_AddNumberToObject(root, JSON_LABEL_PROCS, info->procs) &&
		   cJSON_AddNumberToObject(root, JSON_LABEL_TOTAL_HIGH_MEM_SIZE, info->totalhigh) &&
		   cJSON_AddNumberToObject(root, JSON_LABEL_TOTAL_HIGH_MEM_FREE, info->freehigh) &&
		   cJSON_AddNumberToObject(root, JSON_LABEL_MEM_UNIT_SIZE, info->mem_unit)))
		return  EXIT_FAILURE;

	cJSON *load_array=cJSON_CreateArray();

	if (!load_array)
		return  EXIT_FAILURE;

	if (!cJSON_AddItemToObject(root, JSON_LABEL_LOADS, load_array))
		return  EXIT_FAILURE;

	for (int i=0; i<3; ++i) {
		cJSON *item=cJSON_CreateNumber(info->loads[i]);
		if (!cJSON_AddItemToArray(load_array, item))
			return  EXIT_FAILURE;
	}

	return EXIT_SUCCESS;
}
