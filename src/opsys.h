/*
 * opsys.h
 *
 *  Created on: Apr 12, 2021
 *      Author: zamek
 */

#ifndef SRC_OPSYS_H_
#define SRC_OPSYS_H_

#include <sys/sysinfo.h>
#include <cjson/cJSON.h>

int os_get_machine_id(char *buffer, int buffer_length);

int os_get_host_name(char *buffer, int buffer_length);

int os_get_sysinfo(struct sysinfo *info);

int os_add_sys_info_json(struct sysinfo *info, cJSON *root);

#endif /* SRC_OPSYS_H_ */
