/*
 * test.c
 *
 *  Created on: Apr 10, 2021
 *      Author: zamek
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <stddef.h>
#include <setjmp.h>
#include <cmocka.h>
#include <cjson/cJSON.h>
#include <unistd.h>
#include "const.h"
#include "util.h"
#include "macros.h"

#include "opsys.h"
#include "config.h"
#include "mqtt.h"

#ifdef TEST

#define TEST_BUFFER_SIZE 64
#define JSON_BUFFER_SIZE 1024

static char test_buffer[TEST_BUFFER_SIZE];
static char json_buffer[JSON_BUFFER_SIZE];
static int test_cfg_load1=3;
static int test_cfg_load5=4;
static int test_cfg_load15=5;
static long test_cfg_free_ram=100000;
static long test_cfg_free_swap=0;
static long test_cfg_max_proc=1000;
static int test_cfg_check_interval_sec=10;
static bool config_received=false;

static void parse_sysinfo_json(struct sysinfo *info) {
	if (!info)
		return;

	bzero(info, sizeof(struct sysinfo));

	cJSON *json=cJSON_Parse(json_buffer);
	if (!json)
		return;

	info->uptime=get_long_json(json, JSON_LABEL_UPTIME);
	info->totalram=get_long_json(json, JSON_LABEL_TOTAL_RAM);
	info->freeram=get_long_json(json, JSON_LABEL_FREE_RAM);
	info->sharedram=get_long_json(json, JSON_LABEL_SHARED_RAM);
	info->totalswap=get_long_json(json, JSON_LABEL_TOTAL_SWAP);
	info->freeswap=get_long_json(json, JSON_LABEL_FREE_SWAP);
	info->procs=get_long_json(json, JSON_LABEL_PROCS);
	info->totalhigh=get_long_json(json, JSON_LABEL_TOTAL_HIGH_MEM_SIZE);
	info->freehigh=get_long_json(json, JSON_LABEL_TOTAL_HIGH_MEM_FREE);
	info->mem_unit=get_long_json(json, JSON_LABEL_MEM_UNIT_SIZE);


	cJSON *loads=cJSON_GetObjectItem(json, JSON_LABEL_LOADS);
	if (!loads)
		goto exit;

	if (!cJSON_IsArray(loads))
		goto exit;

	cJSON *item;
	int i=0;
	cJSON_ArrayForEach(item, loads) {
		if (i>2)
			goto exit;
		info->loads[i++]=cJSON_GetNumberValue(item);
	}

exit:
	cJSON_Delete(json);
}

static void test_machine_id() {
	LOGD("Enter test_machine_id");
	assert_int_equal(os_get_machine_id(test_buffer, TEST_BUFFER_SIZE), EXIT_SUCCESS);
	LOGD("Finished test_machine_id");
}

static void test_host_name() {
	LOGD("Enter test_host_name");
	assert_int_equal(os_get_host_name(test_buffer, TEST_BUFFER_SIZE), EXIT_SUCCESS);
	LOGD("Finished test_host_name");
}

static void test_sysinfo() {
	LOGD("Enter test_sysinfo");
	struct sysinfo info;
	assert_int_equal(os_get_sysinfo(&info), EXIT_SUCCESS);
	cJSON *root=cJSON_CreateObject();

	assert_int_equal(os_add_sys_info_json(&info, root), EXIT_SUCCESS);

	cJSON_PrintPreallocated(root, json_buffer, JSON_BUFFER_SIZE, 1);

	struct sysinfo parsed_info;
	parse_sysinfo_json(&parsed_info);
	assert_int_equal(info.uptime, parsed_info.uptime);
	assert_int_equal(info.totalram, parsed_info.totalram);
	assert_int_equal(info.freeram, parsed_info.freeram);
	assert_int_equal(info.sharedram, parsed_info.sharedram);
	assert_int_equal(info.totalswap, parsed_info.totalswap);
	assert_int_equal(info.freeswap, parsed_info.freeswap);
	assert_int_equal(info.procs, parsed_info.procs);
	assert_int_equal(info.totalhigh, parsed_info.totalhigh);
	assert_int_equal(info.freehigh, parsed_info.freehigh);
	assert_int_equal(info.mem_unit, parsed_info.mem_unit);
	assert_memory_equal(info.loads, parsed_info.loads, sizeof(parsed_info.loads));
	cJSON_Delete(root);
	LOGD("Finished test_sysinfo");
}

static void test_config() {
	LOGD("Enter test config");
	const char *CFG_FILE="test.cfg";
	const char *KEY_INT="key_int";
	const char *KEY_LONG="key_long";
	const char *KEY_LONG_LONG="key_long_long";
	const char *KEY_FLOAT="key_float";
	const char *KEY_DOUBLE="key_double";
	const char *KEY_STRING="key_string";
	const char *ZAPHOD="Zaphod";
	const char *TRICIA="Tricia";
	unlink(CFG_FILE);
	assert_int_equal(cfg_init(CFG_FILE, true), EXIT_SUCCESS);
	assert_int_equal(cfg_get_int(KEY_INT, 42), 42);
	assert_true(cfg_get_long(KEY_LONG, 42)==(long)42);
	assert_true(cfg_get_long_long(KEY_LONG_LONG, 42)==(long long)42);
	assert_float_equal(cfg_get_float(KEY_FLOAT, 42.0), 42.0, 0.1);
	assert_float_equal(cfg_get_double(KEY_DOUBLE, 42.0), 42.0, 0.1);
	char str[16];
	cfg_get_string(KEY_STRING, str, 16, ZAPHOD);
	assert_int_equal(strcmp(str, ZAPHOD), 0);

	assert_int_equal(cfg_set_int(KEY_INT, 43), EXIT_SUCCESS);
	assert_int_equal(cfg_set_long(KEY_LONG, 44), EXIT_SUCCESS);
	assert_int_equal(cfg_set_long_long(KEY_LONG_LONG, 45), EXIT_SUCCESS);
	assert_int_equal(cfg_set_float(KEY_FLOAT, 46.0), EXIT_SUCCESS);
	assert_int_equal(cfg_set_double(KEY_DOUBLE, 47.0), EXIT_SUCCESS);
	assert_int_equal(cfg_set_string(KEY_STRING, TRICIA, 16), EXIT_SUCCESS);

	assert_int_equal(cfg_get_int(KEY_INT, 42), 43);
	assert_true(cfg_get_long(KEY_LONG, 42)==(long)44);
	assert_true(cfg_get_long_long(KEY_LONG_LONG, 42)==(long long)45);
	assert_float_equal(cfg_get_float(KEY_FLOAT, 42.0), 46.0, 0.1);
	assert_float_equal(cfg_get_double(KEY_DOUBLE, 42.0), 47.0, 0.1);
	cfg_get_string(KEY_STRING, str, 16, ZAPHOD);
	assert_int_equal(strcmp(str, TRICIA), 0);

	assert_int_equal(cfg_save(CFG_FILE), EXIT_SUCCESS);
	assert_int_equal(cfg_deinit(), EXIT_SUCCESS);

	assert_int_equal(cfg_init(CFG_FILE, false), EXIT_SUCCESS);
	assert_int_equal(cfg_get_int(KEY_INT, 42), 43);
	assert_true(cfg_get_long(KEY_LONG, 42)==(long)44);
	assert_true(cfg_get_long_long(KEY_LONG_LONG, 42)==(long long)45);
	assert_float_equal(cfg_get_float(KEY_FLOAT, 42.0), 46.0, 0.1);
	assert_float_equal(cfg_get_double(KEY_DOUBLE, 42.0), 47.0, 0.1);
	cfg_get_string(KEY_STRING, str, 16, ZAPHOD);
	assert_int_equal(strcmp(str, TRICIA), 0);


	LOGD("Finished test config");
}

void create_and_send_test_config() {
        cJSON *cfg=cJSON_CreateObject();

        if(!(cJSON_AddNumberToObject(cfg, JSON_LABEL_LOAD1, test_cfg_load1) &&
                cJSON_AddNumberToObject(cfg, JSON_LABEL_LOAD5, test_cfg_load5) &&
                cJSON_AddNumberToObject(cfg, JSON_LABEL_LOAD15, test_cfg_load15) &&
                cJSON_AddNumberToObject(cfg, JSON_LABEL_FREE_RAM, test_cfg_free_ram) &&
                cJSON_AddNumberToObject(cfg, JSON_LABEL_FREE_SWAP, test_cfg_free_swap) &&
                cJSON_AddNumberToObject(cfg, JSON_LABEL_PROCS, test_cfg_max_proc) &&
                cJSON_AddNumberToObject(cfg, JSON_LABEL_CHECK_INTERVAL_SEC, test_cfg_check_interval_sec)))
                goto error_exit;

        cJSON_PrintPreallocated(cfg, json_buffer, JSON_BUFFER_SIZE , 1);
        assert_true(mqtt_send_topic(TOPIC_CONFIG, json_buffer)==EXIT_SUCCESS);

error_exit:
        cJSON_Delete(cfg);
}

int test_subscribe_callback(const char *topic, char * payload, int payload_length) {
        assert_true(topic);
        assert_true(payload);
        assert_int_not_equal(payload_length, 0);
        config_t config;
        assert_true(strcmp(topic, TOPIC_CONFIG)==0);
        assert_int_equal(process_config(&config, payload, payload_length), EXIT_SUCCESS);
        assert_true(config.check_interval_sec==test_cfg_check_interval_sec);
        assert_true(config.max_load1==test_cfg_load1);
        assert_true(config.max_load5==test_cfg_load5);
        assert_true(config.max_load15==test_cfg_load15);
        assert_true(config.min_free_ram==test_cfg_free_ram);
        assert_true(config.min_free_swap==test_cfg_free_swap);
        assert_true(config.max_processes==test_cfg_max_proc);

        return EXIT_SUCCESS;
}

void test_mqtt() {
        LOGD("test_mqtt started");
        assert_int_equal(mqtt_init(), EXIT_SUCCESS);
        assert_int_equal(mqtt_set_subscribe_callback(test_subscribe_callback), EXIT_SUCCESS);
        create_and_send_test_config();
        sleep(10);
        assert_int_equal(mqtt_deinit(), EXIT_SUCCESS);
        assert_true(config_received);
        LOGD("test_mqtt finished");
}
int test(void) {
	const struct CMUnitTest tests[] = {
		cmocka_unit_test(test_machine_id),
		cmocka_unit_test(test_host_name),
		cmocka_unit_test(test_sysinfo),
		cmocka_unit_test(test_config),
		cmocka_unit_test(test_mqtt)
	};

	return cmocka_run_group_tests(tests, NULL, NULL);
}

#endif
