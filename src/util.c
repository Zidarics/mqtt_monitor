/*
 * util.c
 *
 *  Created on: Apr 15, 2021
 *      Author: zamek
 */

#include <stdlib.h>
#include <syslog.h>

#include "util.h"
#include "mqtt.h"
#include "const.h"

char *chomp(char *str) {
	if (!str)
		return str;

	char *b=str;
	while(*str++)
		;

	while (--str>b) {
		if (*str>=' ')
			return b;
		*str='\0';
	}
	return b;
}

#define CHECK_RANGE(val,min,max,def) 				\
	do {											\
		if (val<min || val>max)						\
			val=def;								\
	} while(0)


/**
  {
  "load1": 10,
  "load5":  12,
  "load15" : 5,
  "freeRam": 10000,
  "freeSwap" : 1000000,
  "processes" : 1000,
  "checkIntervalSec" : 15
  }

 */
int process_config(config_t *config, char * payload, int payload_length) {
	syslog(LOG_DEBUG, "Enter process_config, %.*s", payload_length, payload);

	if (!(config&&payload&&payload_length))
		return EXIT_FAILURE;

	cJSON *json=cJSON_Parse(payload);
	if (!json)
		return EXIT_FAILURE;

	config->max_load1=get_int_json(json, JSON_LABEL_LOAD1);
	config->max_load5=get_int_json(json, JSON_LABEL_LOAD5);
	config->max_load15=get_int_json(json, JSON_LABEL_LOAD15);

	config->min_free_ram=get_long_json(json, JSON_LABEL_FREE_RAM);
	config->min_free_swap=get_long_json(json, JSON_LABEL_FREE_SWAP);

	config->max_processes=get_int_json(json, JSON_LABEL_PROCS);
	config->check_interval_sec=get_int_json(json, JSON_LABEL_CHECK_INTERVAL_SEC);

	CHECK_RANGE(config->max_load1, MIN_MAX_LOAD1, MAX_MAX_LOAD1, DEF_MAX_LOAD1);
	CHECK_RANGE(config->max_load5, MIN_MAX_LOAD5, MAX_MAX_LOAD5, DEF_MAX_LOAD5);
	CHECK_RANGE(config->max_load15, MIN_MAX_LOAD15, MAX_MAX_LOAD15, DEF_MAX_LOAD15);
	CHECK_RANGE(config->min_free_ram, MIN_MIN_FREE_RAM, MAX_MIN_FREE_RAM, DEF_MIN_FREE_RAM);
	CHECK_RANGE(config->min_free_swap, MIN_MIN_FREE_SWAP, MAX_MIN_FREE_SWAP, DEF_MIN_FREE_SWAP);
	CHECK_RANGE(config->min_free_ram, MIN_MIN_FREE_RAM, MAX_MIN_FREE_RAM, DEF_MIN_FREE_RAM);
	CHECK_RANGE(config->max_processes, MIN_MAX_PROCESSES, MAX_MAX_PROCESSES, DEF_MAX_PROCESSES);

	cJSON_Delete(json);
	return EXIT_SUCCESS;
}
