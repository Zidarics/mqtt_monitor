/*
 * util.h
 *
 *  Created on: Apr 15, 2021
 *      Author: zamek
 */

#ifndef SRC_UTIL_H_
#define SRC_UTIL_H_

#include <cjson/cJSON.h>
#include "const.h"

char *chomp(char *str);

static inline int get_int_json(cJSON *root, char *label) {
	cJSON *item=cJSON_GetObjectItem(root, label);
	return item? (int) cJSON_GetNumberValue(item) : 0;
}

static inline long get_long_json(cJSON *root, char *label) {
	cJSON *item=cJSON_GetObjectItem(root, label);
	return item? (long) cJSON_GetNumberValue(item) : 0;
}

int process_config(config_t *config, char * payload, int payload_length);

#endif /* SRC_UTIL_H_ */
